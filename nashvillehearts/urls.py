from django.conf.urls import url
from django.contrib.auth.views import auth_login

from .views import (ProviderList, ProviderDetail,
                    LocationList, LocationDetail,
                    EventList, EventDetail,
                    UrgentNeedList, UrgentNeedDetail,
                    ServiceList, ClientTypeList, LanguageList, FaqList)



urlpatterns = [
    url(r'^providers/$', ProviderList.as_view(), name='provider-list'),
    url(r'^providers/(?P<pk>[\d]+)$', ProviderDetail.as_view(), name='provider-detail'),
    url(r'^resource-summary/(?P<slug>[\w]+)$', ProviderDetail.as_view(), name='provider-detail'),
    url(r'^locations/$', LocationList.as_view(), name='location-list'),
    url(r'^locations/(?P<pk>[\d]+)$', LocationDetail.as_view(), name='location-detail'),
    url(r'^events/$', EventList.as_view(), name='event-list'),
    url(r'^events/(?P<pk>[\d]+)$', EventDetail.as_view(), name='event-detail'),
    url(r'^urgent-needs/$', UrgentNeedList.as_view(), name='urgent-need-list'),
    url(r'^urgent-needs/(?P<pk>[\d]+)$', UrgentNeedDetail.as_view(), name='urgent-need-detail'),
    url(r'^services/$', ServiceList.as_view(), name='service-list'),
    url(r'^client-types/$', ClientTypeList.as_view(), name='client-type-list'),
    url(r'^languages/$', LanguageList.as_view(), name='language-list'),
    url(r'^faq/$', FaqList.as_view(), name='faq-list'),
]

