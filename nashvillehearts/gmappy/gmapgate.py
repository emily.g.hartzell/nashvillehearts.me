import googlemaps
import datetime

from django.conf import settings


class GmapGate:

    def __init__(self):
        if not settings.GMAP_KEY:
            raise RuntimeError("Missing GMAP_KEY in settings file!")

        self.gkey = settings.GMAP_KEY
        self.gmap = googlemaps.Client(key=self.gkey)

    def geocode(self, address):
        res = self.gmap.geocode(address)

        if len(res) > 0:
            res = res.pop(0)
            lat = res["geometry"]["location"]["lat"]
            lng = res["geometry"]["location"]["lng"]
            return lat, lng
        print(res)
        return None
