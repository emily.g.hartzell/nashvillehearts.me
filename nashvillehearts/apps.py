from django.apps import AppConfig


class NashvilleheartsConfig(AppConfig):
    name = 'nashvillehearts'
